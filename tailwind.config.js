module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  corePlugins: {
    preflight: true,
  },
  theme: {
    extend: {
        colors: {
          'az-primary': '#64c900',
          'az-secondary': '#047c04',
          'az-gray': '#6c6c6c',
          'az-dark': '#2c2c2b',
        }
    },
  },
  variants: {
      borderRadius: ['responsive', 'hover', 'focus'],
      display: ['responsive', 'hover', 'group-hover'],
      translate: ['responsive', 'hover', 'group-hover' ,'focus', 'motion-safe'],
      opacity: ['responsive', 'hover', 'focus', 'group-hover', 'disabled'],
      transitionProperty: ['responsive', 'hover', 'focus'],
      height: ['responsive'],
      width: ['responsive'],
      scale: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
};
