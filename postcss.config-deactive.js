module.exports = {
    entry: "./src/css/main.css",
    plugins: [
        require("tailwindcss"), 
        require("autoprefixer"),
        require("@fullhuman/postcss-purgecss")({
            content: [
                './*.html',
                '../../../application/views/new-theme/*.php',
                '../../../application/views/new-theme/layout/*.php',
            ],
            defaultExtractor: content => content.match([/[A-Za-z0-9-_:/]+/g]) || []
        })
    ]
};
