/**
 * jQuery PageLoad
 *
 * Copyright 2016, Artlogic Media Ltd, http://www.artlogic.net/
 */


 (function($) {

    $.pageload = function(user_options) {
        // Options setup

        var options = {
            'splash_screen_enabled': false,
            'splash_screen_always_enabled': false,
            'splash_screen_cookie_expiry': 900,
            'popup_content_enabled': false,
            'popup_content_update_url': true,
            'ajax_navigation_enabled': true,
            'development_mode': false,
            'development_mode_pause_splash_screen': false,
            'loader_orientation': 'width',
            'verbose': false,
            'content_area_selector': '#container_inner',
            'preloader_additional_html': '',
            'body_classes_to_retain': '',
            'splash_screen_primary_preload_images_selector': '#loader_logo', // Previously splash_logo_selector
            'splash_screen_secondary_preload_images_selector': '#preloader', // Previously splash_background_selector
            'splash_screen_delay_after_complete': 200,
            'page_transition_delay': 600,
            'preload_images_selector': '',
            'destroy_after_load': function() {},
            'init_after_load': function() {
                if(window.galleries) {
                    window.galleries.init();
                }
                if(window.cart) {
                    window.cart.init();
                }
                if(window.site) {
                    window.site.init();
                }
            },
            'before_load': function() {
                if ($('body').hasClass('navigation-open')) {
                    $('body').removeClass('navigation-open');
                    $('#top_nav_section').fadeOut(500);
                    $('#top_nav_section_reveal').removeClass('open');
                    $('#top_nav_section li').removeClass('show');
                }
            },
            'after_load_complete': function() {},
            'preloader_in': function() {},
            'splash_screen_in': function() {},
            'splash_screen_out': function() {},
            'popup_after': function() {},
            'popup_after_close': function() {}
        };
        $.extend(options, user_options);
        $.pageload.options = options;


        // Setup aliases

        $.pageload.load = function(element_href, pushstate, callback, navigation_type) {
            $.pageload.fn.pages.load(element_href, pushstate, callback, navigation_type);
        }

        $.pageload.refresh = function(element_context) {
            $.pageload.fn.pages.add_click_events(element_context);
        }

        $.pageload.popup_close = function(forced) {
            var forced = forced && typeof forced != 'undefined' ? true : false;
            $.pageload.fn.popup.close_popup(forced);
        }


        // Functions

        $.pageload.fn = {

            init: function() {
                if ($.pageload.options.popup_content_enabled) {
                    $.pageload.fn.popup.init();
                }
                $.pageload.fn.preloader.init();
                $.pageload.fn.pages.init();
            },

            _fetchBackgroundImages: function(images_to_load_selector) {
                $(images_to_load_selector).length === 0 ? console.warn('images_to_load_selector returned none') : "";
                // Put background images in place to initiate load
                $(images_to_load_selector).each(function() {
                    if ($(this).filter('[data-style]').length) {
                        var new_style_value = $(this).attr('data-style');
                        if ($(this).attr('style') && typeof $(this).attr('style') != 'undefined') {
                            var new_style_value = new_style_value + $(this).attr('style');
                            // console.log(new_style_value);
                        }
                        $(this).attr('style', new_style_value);
                        $(this).removeAttr('data-style');
                    }
                });
            },

            preloader: {

                init: function() {
                    var preloader_html = '<div id="preloader"><div id="preloader_mask"></div>' + $.pageload.options.preloader_additional_html + '<div id="loader_wrapper"><div id="loader"></div></div><div id="loader_logo"></div></div>';

                    var first_page_load = $('body').hasClass('first-load-complete');
                    var internal_link = document.referrer && typeof document.referrer != 'undefined' ? true : false;
                    // var show_splash_screen = ($.pageload.options.splash_screen_always_enabled && !internal_link) || ($.pageload.options.splash_screen_enabled && !h.getCookie('jquery_pageload_recent_visit')); // (!h.getCookie('jquery_pageload_recent_visit'))
                    //Hilman
                    var show_splash_screen = ($.pageload.options.splash_screen_always_enabled)

                    // Force splash screen in development mode
                    if ($.pageload.options.development_mode || window.location.search == '?pageloaddevmode=1') {
                        var show_splash_screen = true;
                    }

                    // Only show the preloader on the first page load
                    if (!first_page_load) {
                        $('body').attr('data-pathname', window.location.pathname + window.location.search);
                        $('body').attr('data-page-title', document.title);
                        if ($('body').find('#preloader').length === 0) {
                            //console.info("Splash screen loader via javascript");
                            $('body').append(preloader_html);
                        }
                        history.replaceState({'firstPageLoad': true}, null, window.location.pathname + window.location.hash + location.search);
                        if (show_splash_screen) {
                            // Show splash screen if its the very first page load, otherwise a cookie is set for 15 minutes and will stop the splash screen showing again during the same website visit
                            $.pageload.fn.preloader.start_load();
                        } else {
                            // If the splash screen is not displayed, only add the first-load-complete class to the body and don't fire the loader script
                            $.pageload.fn.preloader.start_load(true);
                        }
                    }

                    // Set a cookie so the splash screen doesn't show again in the same visit
                    // h.setCookie('jquery_pageload_recent_visit', 'true', '', $.pageload.options.splash_screen_cookie_expiry); // Set cookie for 15 minutes

                },

                start_load: function(short_load) {

                    // Start loader
                    if (short_load) {
                        $('body').addClass('loader-active first-load-complete skip-splash-screen content-load-waiting');
                        $('body')
                        .addClass('loader-active ajax-links-disabled')
                        .delay(50)
                        .queue(function() {
                            $('body').addClass('ajax-loading ajax-initial-loading');
                            $(this).dequeue();
                        })
                        ;
                        $.pageload.fn.preloader.preload_images(short_load);
                    } else {
                        $('body').addClass('loader-active splash-loader-active content-load-waiting');

                        $.pageload.options.preloader_in();

                        $('#preloader')
                        .delay(20)
                        .queue(function() {
                            $(this).addClass('loading-start');
                            $(this).dequeue();
                        })
                        .delay(200)
                        .queue(function() {
                            $(this).addClass('loading-bar-start');
                            $(this).dequeue();
                        })
                        .delay(300)
                        .queue(function() {
                            $(this).dequeue();
                            $.pageload.fn.preloader.preload_images();
                        })
                        ;
                    }

                },

                preload_images: function(short_load) {

                    // Define the images/backgrounds which should be preloaded on first page load
                    // We preload the first two images of the hero slider, and the generic sidebar image for the full detail page of each record

                    var images_to_load_selector = $.pageload.options.preload_images_selector;

                    if ($(images_to_load_selector).length) {

                        // IMPORTANT: Images to be preloaded must be supplied as a selector string to the 'mutliple_elements' parameter.
                        // Otherwise if you use the normal method the script will go through each of the selector items individually and the 'progress' and 'done' callbacks will NOT be run in one single process.
                        // $('body').pageLoadwaitForImages({
                        //     waitForAll: true,
                        //     multiple_elements: images_to_load_selector
                        // });

                        // Check the logo image is loaded before displaying the splash screen.
                        // If there isn't a logo image it should fall through to done.

                        // Put logo image in place to initiate load
                        $.pageload.fn._fetchBackgroundImages($.pageload.options.splash_screen_primary_preload_images_selector);
                        $('body').pageLoadwaitForImages({
                            waitForAll: true,
                            multiple_elements: $.pageload.options.splash_screen_primary_preload_images_selector
                        }).done(function(e){
                            $($.pageload.options.splash_screen_primary_preload_images_selector).css('opacity', 1);

                            if ($.pageload.options.verbose) {
                                console.log('#########');
                                console.log('Preloading images:');
                            }
                            percentCounter = 0;
                            percentIncrements = (100 / $(images_to_load_selector).length);


                            // Put background images in place to initiate load
                            $.pageload.fn._fetchBackgroundImages(images_to_load_selector);
                            // console.log()

                            $('body').pageLoadwaitForImages({
                                waitForAll: true,
                                multiple_elements: images_to_load_selector
                            })
                            .progress(function(loaded, count, success) {
                                percentIncrements = (100 / count);
                                percentCounter = percentCounter + percentIncrements;
                                var currentPercentage = percentCounter + '%';
                                $('#loader').css($.pageload.options.loader_orientation, currentPercentage);
                                    // Connect stuff to the loader here!!!
                                    // var superpathPercentage = 327.86 * (Math.round(percentCounter) / 100);
                                    // $('.superpath').css('stroke-dashoffset', Math.round(327.86 - superpathPercentage));

                                    if ($.pageload.options.verbose) {
                                        console.log('#########');
                                        console.log('Status:');
                                        console.log(currentPercentage);
                                    }
                                })
                            .done(function() {
                                if ($.pageload.options.verbose) {
                                    console.log('#########');
                                    console.log('Status:');
                                    console.log('complete');
                                }

                                $('#loader')
                                .delay(500)
                                .queue(function() {
                                    $.pageload.fn.preloader.load_complete(short_load);
                                    $(this).dequeue();
                                })
                                ;
                            });
                            ;

                            // Fade in the background image once it's finished downloading.
                            // Put logo image in place to initiate load
                            $.pageload.fn._fetchBackgroundImages($.pageload.options.splash_screen_secondary_preload_images_selector);
                            // This has no effect if the page content is loaded before the background image, this is by design!
                            $('body').pageLoadwaitForImages({
                                waitForAll: true,
                                multiple_elements: $.pageload.options.splash_screen_secondary_preload_images_selector
                            })
                            .done(function(e){
                                $('#home_splash_image_container').css({
                                    'opacity': 1
                                });
                            });


                        });
                    } else {
                        // Fade in the background image once it's finished downloading.
                        // Put logo image in place to initiate load
                        $.pageload.fn._fetchBackgroundImages($.pageload.options.splash_screen_secondary_preload_images_selector);
                        // This has no effect if the page content is loaded before the background image, this is by design!
                        $('body').pageLoadwaitForImages({
                            waitForAll: true,
                            multiple_elements: $.pageload.options.splash_screen_secondary_preload_images_selector
                        })
                        .done(function(e){
                            $('#bk-img-container').css('opacity', 1);
                            $('#home_splash_image_container').css({
                                'opacity': 1
                            });
                        });

                        $('#loader')
                        .delay(100)
                        .queue(function() {
                            $(this).css($.pageload.options.loader_orientation, '100%');
                            $(this).dequeue();
                        })
                        .delay(300)
                        .queue(function() {
                            $.pageload.fn.preloader.load_complete(short_load);
                            $(this).dequeue();
                        })
                        ;
                    }
                },

                load_complete: function(short_load) {
                    // Finish the preloader animations

                    if (short_load) {

                        $('body').removeClass('content-load-waiting skip-splash-screen');
                        $.pageload.fn.preloader.end_ajax();

                    } else {
                        if (!$('body').hasClass('first-load-complete')) {
                            if ($.pageload.options.development_mode) {
                                $('#preloader').click(function() {
                                    $(this).dequeue();
                                });
                            }
                            $('#preloader')
                            .queue(function() {
                                $(this).addClass('loading-complete');
                                    $('#loader').css($.pageload.options.loader_orientation, '100.1%'); // Important: Setting the width as 100.1 (or anything other than 100) forces CSS transitions to update if one has specifically been set for .loading-complete (e.g. we usually speed up the progress bar at the end)
                                    if (!$.pageload.options.development_mode) {
                                        $(this).dequeue();
                                    }
                                })
                            .delay(400)
                            .queue(function() {
                                $(this).addClass('loading-bar-end');
                                $('#loader').css($.pageload.options.loader_orientation, '');
                                if (!$.pageload.options.development_mode) {
                                    $(this).dequeue();
                                }
                            })
                            .delay(500)
                            .queue(function() {
                                $(this).addClass('splash-screen-in');
                                $.pageload.options.splash_screen_in();
                                if (!$.pageload.options.development_mode && !$.pageload.options.development_mode_pause_splash_screen) {
                                    $(this).dequeue();
                                }
                            })
                            .delay($.pageload.options.splash_screen_delay_after_complete)
                            .queue(function() {
                                $(this).addClass('splash-screen-out');
                                $.pageload.options.splash_screen_out();
                                $('body').removeClass('content-load-waiting');
                                if (!$.pageload.options.development_mode) {
                                    $(this).dequeue();
                                }
                            })
                            .delay(400)
                            .queue(function() {
                                $('body').addClass('first-load-complete');
                                $('body').removeClass('loader-active ajax-loading splash-loader-active');
                                $(this).removeClass('loading-start loading-bar-start loading-complete loading-bar-end splash-screen-in splash-screen-out');
                                $('#loader').css($.pageload.options.loader_orientation, '');
                                $(this).dequeue();
                            })
                            ;
                        }
                    }

                    // After preload has completed, load the rest of the hero images (only the first 2 are preloaded)

                    $('#hero_slider .hero_item .image div[data-style]').each(function() {
                        $(this).attr('style', $(this).attr('data-style'));
                        $(this).removeAttr('data-style');
                    });

                },

                start_ajax: function() {

                    // Reset some things first, if the end of the last load has not completely finished
                    $('body').removeClass('loader-active ajax-loading-complete');
                    $(this).removeClass('loading-start loading-bar-start loading-complete loading-bar-end splash-screen-in splash-screen-out');
                    $('#loader').css($.pageload.options.loader_orientation, '');

                    // Start loading of new page
                    $('body')
                    .clearQueue()
                    .addClass('loader-active ajax-links-disabled page-transition-active')
                    .delay(50)
                    .queue(function() {
                        $('body').addClass('ajax-loading ajax-initial-loading');
                        $(this).dequeue();
                    })
                    ;
                    $('body')
                    .clearQueue('pageload-disable-links')
                    .addClass('pageload-links-disabled-temporarily')
                    .delay(500, 'pageload-disable-links')
                    .queue('pageload-disable-links', function() {
                        $('body').removeClass('pageload-links-disabled-temporarily');
                        $(this).dequeue('pageload-disable-links');
                    })
                    ;


                },

                end_ajax: function() {
                    $('body')
                    .removeClass('ajax-initial-loading ajax-links-disabled pageload-links-disabled-temporarily')
                    .removeClass('page-transition page-transition2')
                    .queue(function() {
                        $('body').removeClass('page-transition ajax-loading').removeClass('page-transition2').addClass('ajax-loading-complete');
                        $(this).dequeue();
                    })
                    .delay(400)
                    .queue(function() {
                        $('body').removeClass('page-transition-active loader-active ajax-loading-complete');
                        $(this).removeClass('complete4').removeClass('loading');
                        $('#loader').css($.pageload.options.loader_orientation, '');
                        $(this).dequeue();
                    })
                    ;

                }

            },

            popup: {

                init: function() {

                    if (!$('#popup_container').length) {
                        $('body').append('<div id="popup_container"><div id="popup_overlay"></div><div id="popup_box"><div class="inner"><div class="close">Close</div><div class="pagination_controls"><div class="previous pagination_controls_previous">Previous</div><div class="next pagination_controls_next">Next</div></div><div id="popup_content"></div></div></div></div>');
                    }

                    if ($('.site-popup-enabled-content').length) {
                        $('body').addClass('site-popup-fallback-mode');
                    } else {
                        $('body').removeClass('site-popup-fallback-mode');
                    }

                    $('#popup_box .close').unbind('click.closefunction').bind('click.closefunction', function() {
                        $.pageload.fn.popup.close_popup();
                    });

                    $('#popup_overlay').click(function() {
                        $('#popup_box .close').trigger('click');
                    });

                },

                close_popup: function(forced) {
                    
                    var forced = forced && typeof forced != 'undefined' ? true : false;
                    if ($('body').hasClass('page-popup-force-close')) {
                        var forced = true;
                    }

                    if (!forced) {
                        $('body').addClass('page-popup-clicked-close');
                        if ($.pageload.options.popup_content_update_url ){
                            window.history.back();
                        }
                    }
                    $('body')
                    .clearQueue()
                    .removeClass('page-popup-visible')
                    .delay(600)
                    .queue(function() {
                        $.pageload.options.popup_after_close();
                            //if (!$('body').hasClass('page-popup-force-close')) {
                            //    history.replaceState({'ajaxPageLoad': true}, null, $('body').attr('data-pathname'));
                            //}

                            $('html').removeClass('page-popup-active');
                            $(this).removeClass('page-popup-active page-popup-force-close page-popup-clicked-close');
                            $(this).dequeue();
                        })
                    ;

                },

                after_load: function() {

                    // Previous/Next buttons

                    $('#popup_box').delay(400).queue(function() {
                        $('body').removeClass('page-popup-transition-initial');
                        $(this).removeClass('popup-direction-left popup-direction-right');
                        $(this).dequeue();
                    });

                    var previous_link = $('#popup_content .pagination_controls .pagination_controls_prev a').attr('href');
                    var next_link = $('#popup_content .pagination_controls .pagination_controls_next a').attr('href');

                    if (previous_link && typeof previous_link != 'undefined') {
                        $('#popup_box > .inner .pagination_controls .previous')
                        .unbind()
                        .attr('data-href', previous_link)
                        .removeClass('disabled')
                        .click(function() {
                            $('body').addClass('page-popup-transition-initial');
                            $('#popup_box').addClass('popup-direction-left');
                            $.pageload.fn.pages.load($(this).attr('data-href'), false, null);
                            return false;
                        })
                        ;
                    } else {
                        $('#popup_box > .inner .pagination_controls .previous').unbind().attr('data-href', '').addClass('disabled');
                    }

                    if (next_link && typeof next_link != 'undefined') {
                        $('#popup_box > .inner .pagination_controls .next')
                        .unbind()
                        .attr('data-href', next_link)
                        .removeClass('disabled')
                        .click(function() {
                            $('body').addClass('page-popup-transition-initial');
                            $('#popup_box').addClass('popup-direction-right');
                            $.pageload.fn.pages.load($(this).attr('data-href'), false, null);
                            return false;
                        })
                        ;
                    } else {
                        $('#popup_box > .inner .pagination_controls .next').unbind().attr('data-href', '').addClass('disabled');
                    }


                    // Run functions required in the popup

                    $.pageload.options.popup_after();

                }

            },

            pages: {

                init: function() {

                    $.pageload.fn.pages.add_click_events();

                    $(window).on("popstate", function (ev) {
                        if (ev.originalEvent.state) {
                            if (ev.originalEvent.state.ajaxPageLoad || ev.originalEvent.state.firstPageLoad) {
                                if (!window.location.hash || typeof window.location.hash == 'undefined') {
                                    if ($('body').hasClass('page-popup-active') || $('body').hasClass('page-popup-clicked-close')) {
                                        if (!$('body').hasClass('page-popup-force-close') && !$('body').hasClass('page-popup-clicked-close')) {
                                            $('body').addClass('page-popup-force-close');
                                            $('#popup_box .close').trigger('click');
                                        }
                                    } else {
                                        $.pageload.fn.pages.load(window.location.pathname + window.location.search, false, null, 'history_change');
                                    }
                                }
                            }
                        }
                    });

                },

                add_click_events: function(element_context) {
                    if (!element_context || typeof element_context == 'undefined') {
                        var element_context = '#container';
                    }
                    
                    if ($.pageload.options.ajax_navigation_enabled){

                        $('a', element_context).unbind('click.pageload').bind('click.pageload', function() {
                            var element = $(this);
                            var element_href = $(this).attr('href');

                            if (element_href) {
                                if (!$(this).hasClass('link-no-ajax') && !$(this).hasClass('page-load-within') && !$('body').hasClass('slider-drag') && element_href != '#' && element_href.indexOf('/') == 0 && element_href.indexOf('#') < 0 && element_href.indexOf('/custom_images/') < 0 && element_href.indexOf('/usr/') < 0 && element.attr('target') != '_blank') {
                                    if ($('body').hasClass('pageload-links-disabled-temporarily')) {
                                        // Links temporarily disabled, this is to avoid double clicks making pages load twice
                                        return false;
                                    } else if ($('body').hasClass('ajax-links-disabled')) {
                                        // Already loading page content, we could also cancel the request and start a new one. Currently we are allowing standard navigation in this case.
                                    } else {
                                        $.pageload.fn.pages.load(element_href, true, null, null, element);
                                        return false;
                                    }
                                } else if ($('body').hasClass('slider-drag')) {
                                    return false;
                                }
                            }
                        });
                    }

                },

                load: function(element_href, pushstate, callback, navigation_type, element) {
                    
                    $.pageload.options.before_load();

                    if (element_href != '#' && element_href.indexOf('/') == 0 && element_href.indexOf('#') < 0 && element_href.indexOf('/custom_images/') < 0 && element_href.indexOf('/usr/') < 0) {
                        if (element_href == '/') {
                            target_top_level = '[href="' + element_href + '"]';
                        } else {
                            if (typeof $(element).attr('data-original-url') != 'undefined') {
                                target_top_level = $(element).attr('data-original-url').split('/')[1];
                                target_top_level = '/' + target_top_level + '/';
                                target_top_level = '[data-original-url^="' + target_top_level + '"]';
                            } else {
                                target_top_level = element_href.split('/')[1];
                                target_top_level = '/' + target_top_level + '/';
                                target_top_level = '[href^="' + target_top_level + '"]';
                            }
                        }

                        $.pageload.fn.preloader.start_ajax();
                        
                        if ($.pageload.options.verbose) {
                            console.log('#########');
                            console.log('Starting page load:');
                            console.log(element_href);
                        }
                        
                        $.ajax({
                            url: element_href,
                            data: {'ajax': true},
                            cache: true,
                            method: 'POST',
                            dataType: 'html',
                            error: function() {
                                h.alert('Sorry, that page could not be loaded. Please try again later.');
                                $.pageload.fn.preloader.end_ajax();
                            },
                            success: function(new_page_html) {
                                if (new_page_html) {
                                    if ($.pageload.options.verbose) {
                                        console.log('#########');
                                        console.log('PAGE LOAD COMPLETE');
                                    }

                                    new_page_html = new_page_html;

                                    var body_classes_remembered = 'browser-js-enabled';
                                    if ($('body').hasClass('first-load-complete')) {
                                        var body_classes_remembered = body_classes_remembered + ' first-load-complete';
                                    }
                                    if ($('body').hasClass('ajax-loading')) {
                                        var body_classes_remembered = body_classes_remembered + ' ajax-loading';
                                    }
                                    if ($('body').hasClass('page-transition-active')) {
                                        var body_classes_remembered = body_classes_remembered + ' page-transition-active';
                                    }
                                    if ($('body').hasClass('loader-active')) {
                                        var body_classes_remembered = body_classes_remembered + ' loader-active';
                                    }
                                    if ($('body').hasClass('overlay-visible')) {
                                        var body_classes_remembered = body_classes_remembered + ' overlay-visible';
                                    }
                                    if ($('body').hasClass('overlay-active')) {
                                        var body_classes_remembered = body_classes_remembered + ' overlay-active';
                                    }
                                    if ($.pageload.options.body_classes_to_retain) {
                                        additional_classes_to_remember = $.pageload.options.body_classes_to_retain.split(',');
                                        for (i = 0; i < additional_classes_to_remember.length; i++) {
                                            var classname = $.trim(additional_classes_to_remember[i]).replace('.', '');
                                            if ($('body').hasClass(classname)) {
                                                var body_classes_remembered = body_classes_remembered + ' ' + classname;
                                            }
                                        }
                                    }

                                    if (false) {
                                        // DEPRICATED STYLE
                                        new_page_title = (new_page_html.match(/<title>(.*?)<\/title>/) ? new_page_html.match(/<title>(.*?)<\/title>/)[1] : '');
                                        new_page_body_class = (new_page_html.match(/body class=\"(.*?)\"/) ? new_page_html.match(/body class=\"(.*?)\"/)[1] : '') + ' ' + body_classes_remembered;
                                        new_page_html = new_page_html.replace(/data-style=/g, 'style=');
                                        new_page_html_body = $.parseHTML('<div id="ajax_container">' + new_page_html.replace(/^[\S\s]*<body[^>]*?>/i, "").replace(/<\/body[\S\s]*$/i, "") + '</div>');
                                    }

                                    // Create 'parsed' version of the incoming HTML so we can interact as a jQuery object
                                    new_page_html_parsed = $.parseHTML(new_page_html);

                                    // Change instances of 'data-style' to 'style' so we can preload these images later on
                                    new_page_html = new_page_html.replace(/data-style=/g, 'style=');

                                    // Grab the title of the new page
                                    new_page_title = $(new_page_html_parsed).filter('title').text();

                                    // Grab meta objects and replicate on this page
                                    $(new_page_html_parsed).filter('meta[property]').each(function() {
                                        var meta_property_attr = $(this).attr('property');
                                        var meta_content_attr = $(this).attr('content');
                                        if (meta_property_attr && typeof meta_property_attr != 'undefined' && typeof meta_content_attr != 'undefined') {
                                            $('meta[property="' + meta_property_attr + '"]').attr('content', meta_content_attr);
                                        }
                                    });

                                    // Grab the body classes of the new page
                                    new_page_body_class = (new_page_html.match(/body class=\"(.*?)\"/) ? new_page_html.match(/body class=\"(.*?)\"/)[1] : '') + ' ' + body_classes_remembered;

                                    // Parse the body of the new page HTML so we can interact with it with jQuery
                                    new_page_html_body = $.parseHTML('<div id="ajax_container">' + new_page_html.replace(/^[\S\s]*<body[^>]*?>/i, "").replace(/<\/body[\S\s]*$/i, "") + '</div>');

                                    if (new_page_html_body.length > 1) {
                                        // This is to fix an issue where more than one element was being returned by new_page_html_body - #ajax_container is the only element we want. Other elements included comment tags.
                                        new_page_html_body = $(new_page_html_body).filter('#ajax_container');
                                    }

                                    // Find the container_inner of the new page content
                                    new_page_main_content = $($.pageload.options.content_area_selector, new_page_html_body).html();

                                    // Find out if the content of the new page is popup enabled content (popup mode)
                                    new_page_popup_mode_enabled = $(new_page_main_content).find('.site-popup-enabled-content').length;
                                    new_page_popup_content = (new_page_popup_mode_enabled ?  $('.site-popup-enabled-content', new_page_html_body).html() : '');

                                    // Find out if the content of the new page can be displayed 'within' the current page (e.g. inner mode)
                                    new_page_inner_mode_enabled = $(new_page_main_content).find('.site-ajax-enabled-inner-content').length;
                                    new_page_inner_content = (new_page_inner_mode_enabled ?  $('.site-ajax-enabled-inner-content', new_page_html_body).html() : '');

                                    // Preload images for new page
                                    images_to_load_selector = $.pageload.options.preload_images_selector;

                                    // Wait for the images to load
                                    if ($(images_to_load_selector, new_page_html_body).length) {
                                        if ($.pageload.options.verbose) {
                                            console.log('#########');
                                            console.log('Preloading images:');
                                        }

                                        percentCounter = 0;
                                        percentIncrements = (100 / $(images_to_load_selector, new_page_html_body).length);

                                        // IMPORTANT: Images to be preloaded must be supplied as a selector string to the 'mutliple_elements' parameter.
                                        // Otherwise if you use the normal method the script will go through each of the selector items individually and the 'progress' and 'done' callbacks will NOT be run in one single process.

                                        $(new_page_html_body).pageLoadwaitForImages({
                                            waitForAll: true,
                                            multiple_elements: images_to_load_selector
                                        })
                                        .progress(function(loaded, count, success) {
                                            percentIncrements = (100 / count);
                                            percentCounter = percentCounter + percentIncrements;
                                            var currentPercentage = percentCounter + '%';
                                            $('#loader').css($.pageload.options.loader_orientation, currentPercentage);

                                            if ($.pageload.options.verbose) {
                                                console.log('#########');
                                                console.log('Status:');
                                                console.log(currentPercentage);
                                            }
                                        })
                                        .done(function() {
                                            if ($.pageload.options.verbose) {
                                                console.log('#########');
                                                console.log('Status:');
                                                console.log('complete');
                                            }
                                            $.pageload.fn.preloader.load_complete();
                                            $.pageload.fn.pages.after_load(new_page_title, new_page_body_class, new_page_html_body, new_page_main_content, new_page_popup_content, target_top_level, element_href, new_page_popup_mode_enabled, new_page_inner_mode_enabled, new_page_inner_content, callback, pushstate, navigation_type);
                                        })
                                        ;
                                    } else {
                                        // No images to preload
                                        if ($.pageload.options.verbose) {
                                            console.log('#########');
                                            console.log('No images to load');
                                        }
                                        setTimeout(function() {
                                            $('#loader').css($.pageload.options.loader_orientation, '100%');
                                            $.pageload.fn.preloader.load_complete();
                                            $.pageload.fn.pages.after_load(new_page_title, new_page_body_class, new_page_html_body, new_page_main_content, new_page_popup_content, target_top_level, element_href, new_page_popup_mode_enabled, new_page_inner_mode_enabled, new_page_inner_content, callback, pushstate, navigation_type);
                                        }, 100);
                                    }

                                }
                            }
                        });
}

},

after_load: function(new_page_title, new_page_body_class, new_page_html_body, new_page_main_content, new_page_popup_content, target_top_level, element_href, new_page_popup_mode_enabled, new_page_inner_mode_enabled, new_page_inner_content, callback, pushstate, navigation_type) {

                    // Show new page content
                    
                    if (new_page_popup_mode_enabled && !$('body').hasClass('site-popup-fallback-mode')) { // Skip this step if we are on a full page where the content usually opens as popup content, continue to open in fallback mode - not in a popup

                        // Popup mode content

                        if (new_page_title) {
                            document.title = new_page_title;
                        }
                        
                        if ($('body').hasClass('page-popup-active')) {
                            // If a popup is already open
                            if ($.pageload.options.popup_content_update_url ){
                                history.replaceState({'ajaxPageLoad': true}, null, element_href);
                            }
                            $('body')
                            .addClass('page-popup-transition')
                            .delay(400)
                            .queue(function() {
                                $('#popup_content').html(new_page_popup_content);
                                $.pageload.fn.popup.after_load();
                                $.pageload.fn.preloader.end_ajax();
                                $(this).addClass('page-popup-transition2');
                                $(this).dequeue();
                            })
                            .delay(10)
                            .queue(function() {
                                $(this).removeClass('page-popup-transition page-popup-transition2');
                                $(this).dequeue();
                            })
                            ;
                        } else {
                            // If there is no existing popup, open a new one
                            if ($.pageload.options.popup_content_update_url ){
                                history.pushState({'ajaxPageLoad': true}, null, element_href);
                            }
                            $('#popup_content').html(new_page_popup_content);
                            $('html').addClass('page-popup-active');
                            $('body')
                            .clearQueue()
                            .addClass('page-popup-active')
                            .removeClass('ajax-initial-loading')
                            .delay(10)
                            .queue(function() {
                                $(this).addClass('page-popup-visible');
                                $(this).dequeue();
                            })
                            ;
                            $.pageload.fn.popup.after_load();
                            $.pageload.fn.preloader.end_ajax();
                        }

                    } else if (new_page_inner_mode_enabled && callback) {

                        // Inner ajax content mode

                        callback(new_page_inner_content, element_href, new_page_main_content);

                        $('body')
                        .clearQueue()
                        .removeClass('ajax-initial-loading')
                        .delay(10)
                        .queue(function() {
                            $(this).dequeue();
                        })
                        ;

                        $.pageload.fn.preloader.end_ajax();

                        if (pushstate) {
                            history.pushState({'ajaxPageLoad': true}, null, element_href);
                        }

                    } else {

                        // Standard page content
                        
                        if (callback && typeof callback != 'null' && typeof callback != 'undefined') {
                            callback();
                        }

                        if (navigation_type == 'history_change') {
                            // If navigation_type has been set as 'history_change', instantly show the content
                            $('body')
                            .clearQueue()
                            .removeClass('ajax-initial-loading')
                            ;
                        } else {
                            // If navigation_type has been set as default, show the page transition effects
                            $('body')
                            .clearQueue()
                            .addClass('page-transition')
                            .removeClass('ajax-initial-loading')
                            .delay($.pageload.options.page_transition_delay)
                            .queue(function() {
                                $(this).addClass('page-transition2');
                                $(this).dequeue();
                            })
                            ;
                        }

                        setTimeout(function() {

                            // Scroll page to top
                            $('html,body').animate(
                                {scrollTop: 0},
                                0,
                                'easeInOutQuad'
                                );

                            $.pageload.options.destroy_after_load();
                            
                            // Unbind all events
                            $(window).unbind();
                            $('#container *, #cms-frontend-toolbar *').unbind().off();

                            // Destroy jQuery plugin instances
                            $('#slideshow ul, #ig_slideshow, .lists ul li .panel_image_slideshow').cycle('destroy');

                            // Update CMS toolbar
                            $('#cms-frontend-toolbar').html($('#cms-frontend-toolbar', new_page_html_body).html());


                            $('#top_nav.navigation ul li').removeClass('active');
                            $('#top_nav.navigation ul li a' + target_top_level + '').parent().addClass('active');
                            $('body').attr('class', new_page_body_class);
                            $($.pageload.options.content_area_selector).html(new_page_main_content).css('margin-top', '');

                            if (pushstate) {
                                history.pushState({'ajaxPageLoad': true}, null, element_href);
                            }
                            
                            if (new_page_title) {
                                document.title = new_page_title;
                            }

                            $('body').attr('data-pathname', window.location.pathname + window.location.search);
                            $('body').attr('data-page-title', document.title);

                            $.pageload.options.init_after_load();

                            if (window.cms_toolbar) {
                                window.cms_toolbar.init();
                            }

                            $.pageload.options.after_load_complete();

                            if (!$('body').hasClass('site-generic-overlay-fallback-mode')) {
                                $('#overlay-close').trigger('click');
                            }

                            $(window).trigger('resize');

                            $.pageload.fn.preloader.end_ajax();

                        }, $.pageload.options.page_transition_delay);
                    }

                    // Track pageview with Analytics if it is installed

                    if (window._gaq || window.ga) {
                        if (window.ga) {
                            // Universal analytics
                            ga('send', 'pageview', element_href);
                            // Send tracker2 pageview if a secondary tracker has been installed
                            ga('tracker2.send', 'pageview', element_href);
                        } else if (window._gaq) {
                            // Legacy analytics
                            _gaq.push(['_trackPageview', element_href]);
                        }
                    }

                }

            }
        };


        // Initialise function

        //$(window).ready(function() {
            $.pageload.fn.init();
        //});

    };

})(window.jQuery);




/**
 * jQuery PageLoadWaitForImages
 *
 * Copyright 2016, Artlogic Media Ltd, http://www.artlogic.net/
 */

 (function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // CommonJS / nodejs module
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    // Namespace all events.
    var eventNamespace = 'pageLoadwaitForImages';

    // CSS properties which contain references to images.
    $.pageLoadwaitForImages = {
        hasImageProperties: [
        'backgroundImage',
        'listStyleImage',
        'borderImage',
        'borderCornerImage',
        'cursor'
        ],
        hasImageAttributes: ['srcset']
    };

    // Custom selector to find all `img` elements with a valid `src` attribute.
    $.expr[':']['has-src'] = function (obj) {
        // Ensure we are dealing with an `img` element with a valid
        // `src` attribute.
        return $(obj).is('img[src][src!=""]');
    };

    // Custom selector to find images which are not already cached by the
    // browser.
    $.expr[':'].uncached = function (obj) {
        // Ensure we are dealing with an `img` element with a valid
        // `src` attribute.
        if (!$(obj).is(':has-src')) {
            return false;
        }

        return !obj.complete;
    };

    $.fn.pageLoadwaitForImages = function () {

        var allImgsLength = 0;
        var allImgsLoaded = 0;
        var deferred = $.Deferred();

        var finishedCallback;
        var eachCallback;
        var waitForAll;
        var multiple_elements;

        // Handle options object (if passed).
        if ($.isPlainObject(arguments[0])) {

            multiple_elements = arguments[0].multiple_elements;
            waitForAll = arguments[0].waitForAll;
            eachCallback = arguments[0].each;
            finishedCallback = arguments[0].finished;

        } else {

            // Handle if using deferred object and only one param was passed in.
            if (arguments.length === 1 && $.type(arguments[0]) === 'boolean') {
                waitForAll = arguments[0];
            } else {
                finishedCallback = arguments[0];
                eachCallback = arguments[1];
                waitForAll = arguments[2];
            }

        }

        // Handle missing callbacks.
        finishedCallback = finishedCallback || $.noop;
        eachCallback = eachCallback || $.noop;

        // Convert waitForAll to Boolean
        waitForAll = !! waitForAll;

        // Ensure callbacks are functions.
        if (!$.isFunction(finishedCallback) || !$.isFunction(eachCallback)) {
            throw new TypeError('An invalid callback was supplied.');
        }

        this.each(function () {
            // Build a list of all imgs, dependent on what images will
            // be considered.
            if (multiple_elements) {
                // If the 'multiple_elements' parameter has been suplied, use this, otherwise use the original element specified
                var obj = $(multiple_elements, this);
            } else {
                var obj = $(this);
            }
            var allImgs = [];
            // CSS properties which may contain an image.
            var hasImgProperties = $.pageLoadwaitForImages.hasImageProperties || [];
            // Element attributes which may contain an image.
            var hasImageAttributes = $.pageLoadwaitForImages.hasImageAttributes || [];
            // To match `url()` references.
            // Spec: http://www.w3.org/TR/CSS2/syndata.html#value-def-uri
            var matchUrl = /url\(\s*(['"]?)(.*?)\1\s*\)/g;

            if (waitForAll) {;
                // Get all elements (including the original), as any one of
                // them could have a background image.
                obj.find('*').addBack().each(function () {
                    var element = $(this);
                    // If an `img` element, add it. But keep iterating in
                    // case it has a background image too.

                    if (element.is('img:has-src') &&
                        !element.is('[srcset]')) {
                        allImgs.push({
                            src: element.attr('src'),
                            element: element[0]
                        });
                }

                $.each(hasImgProperties, function (i, property) {
                    var propertyValue = element.css(property);
                    var match;
                        // If it doesn't contain this property, skip.
                        if (!propertyValue) {
                            return true;
                        }

                        // Get all url() of this element.
                        while (match = matchUrl.exec(propertyValue)) {
                            allImgs.push({
                                src: match[2],
                                element: element[0]
                            });
                        }
                    });

                $.each(hasImageAttributes, function (i, attribute) {
                    var attributeValue = element.attr(attribute);
                    var attributeValues;

                        // If it doesn't contain this property, skip.
                        if (!attributeValue) {
                            return true;
                        }

                        allImgs.push({
                            src: element.attr('src'),
                            srcset: element.attr('srcset'),
                            element: element[0]
                        });

                    });

            });
            } else {
                // For images only, the task is simpler.
                obj.find('img:has-src')
                .each(function () {
                    allImgs.push({
                        src: this.src,
                        element: this
                    });
                });
            }

            window.setTimeout(function() {
                allImgsLength = allImgs.length;
                allImgsLoaded = 0;


                if (allImgsLength === 0) {
                    finishedCallback.call(obj[0]);
                    deferred.resolveWith(obj[0]);
                }

                $.each(allImgs, function (i, img) {

                    var image = new Image();
                    var events =
                    'load.' + eventNamespace + ' error.' + eventNamespace;

                    // Handle the image loading and error with the same callback.
                    $(image).one(events, function me (event) {
                        // If an error occurred with loading the image, set the
                        // third argument accordingly.
                        var eachArguments = [
                        allImgsLoaded,
                        allImgsLength,
                        event.type == 'load'
                        ];
                        allImgsLoaded++;

                        eachCallback.apply(img.element, eachArguments);
                        deferred.notifyWith(img.element, eachArguments);

                        // Unbind the event listeners. I use this in addition to
                        // `one` as one of those events won't be called (either
                        // 'load' or 'error' will be called).
                        $(this).off(events, me);

                        if (allImgsLoaded == allImgsLength) {
                            finishedCallback.call(obj[0]);
                            deferred.resolveWith(obj[0]);
                            return false;
                        }

                    });

                    if (img.srcset) {
                        image.srcset = img.srcset;
                    }
                    image.src = img.src;
                });
            }, 0);
        });

return deferred.promise();

};
}));
