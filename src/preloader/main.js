(function($) {

	window.site = {

		init: function() {
			window.site.pageload.init();
			

		},

		pageload: {

			init: function() {
				if ($('body').hasClass('section-error')) {
					$('body').addClass('first-load-complete');
					return false;
				}
				if ('scrollRestoration' in history) {
					history.scrollRestoration = 'auto';
				}
				$.pageload({
					'splash_screen_enabled': true,
					'splash_screen_always_enabled': true,
           			'splash_screen_cookie_expiry': 300, // 5m
           			'splash_screen_delay_after_complete': 2000,
                    //'page_transition_delay': 1200,
                    'page_transition_delay': 800,
                    'popup_content_enabled': true,
                    'development_mode': false,
                    'verbose': false,
                    'content_area_selector': '#main_content',

                    'splash_screen_primary_preload_images_selector': '',
                    'preload_images_selector': '',
                    'preload_videos_selector': '',
                    'destroy_after_load': function() {

                    	if ($('body').hasClass('page-popup-active')) {
	                        // A popup is open, close it
	                        $.pageload.popup_close(true);
	                    }

	                    $('video').remove();
	                    $('.image_gallery_multiple').cycle('destroy');
	                    $('#main_slideshow').cycle('destroy');
	                    $('#main_slideshow').stop().clearQueue().unbind();
	                    window.site.artlogic_inview.destroy();
	                },
	                'popup_after': function(original_click_element) {
	                	window.site.pageload.popup_after_open(original_click_element);
	                },
	                'popup_after_close': function() {
	                	window.site.pageload.popup_after_close();
	                },
	                'before_load': function() {
	                	if ($('.search-overlay').hasClass('active') && !$('.search-overlay').hasClass('search-loading')) {
	                		window.site.overlays.search.close();
	                	}
	                	if ($('.enquire-overlay').hasClass('active')) {
	                		window.site.overlays.enquire.close();
	                	}
	                },
	                'preloader_in': function(short_load) {
	                	if (!short_load) {
	                		window.site.splash_screen.init();
	                	}
	                },
	                'init_after_splash_screen': function() {

	                }
	            });
				$('.nav-overlay .nav-wrapper ul li a').unbind().click(function() {
					$('.nav-close-button').trigger('click');
					$.pageload.load($(this).attr('href'), true, false);
					return false;
				});
				$('#sqcw_checkout a').unbind().click(function() {
					$.pageload.load($(this).attr('href'), true, function() {
						$('#sqcw_close').trigger('click');
					});
					return false;
				});
			},

			close_popup: function() {
				window.history.back();
                //$('#popup_box > .inner > .close').trigger('click');
                $('body').removeClass('content-reversed content-not-reversed');
                $('body').removeClass('user-distraction-free')
                $('body').removeClass('page-artwork-detail-standard');
                $('#popup_box').animate({'scrollTop': 0}, 600);
                $('#popup_content').html('');
            },

            popup_after_open: function(original_click_element) {
            	$('body').removeClass('user-inactive user-idle window-scrolled');
            	$('#container').removeClass('scrolling-down scrolling-up');
            	window.cart.init();
            	window.cart.cart_summary.get_summary(false);

            	window.site.social_media.init();
            	window.site.overlays.enquire.init();
            	window.site.misc.init();

            	window.site.lazy_load.setup();
            	window.site.lazy_load.fire('#popup_box .inview_element');

            	window.site.artworks.setup();

            	$(window).trigger('resize');
            },

            popup_after_close: function() {
            	$('body').removeClass('popup-hidable-content user-distraction-free');
            	$('body').removeClass('content-reversed content-not-reversed');
            	$('#popup_box .artwork_detail.artwork_detail_variant_video .video').html('');
            	$('.image_gallery_multiple').cycle('destroy');
            }

        },


        splash_screen: {
        	init: function() {
        		window.setTimeout(function() {
        			$('body').addClass('active-init');
        			$('body').addClass('splash-init');
        		}, 10);
        		window.setTimeout(function() {
        			$('.splash-bg').addClass('open start');
        			$('body').addClass('splash-open');
        		}, 50);
        		window.setTimeout(function() {
        			window.site.splash_screen.fade_in_letters();
        			window.site.splash_screen.randomise_letters();
        		}, 400);

                //HILMAN
                // $('.container_outer').hide()

            },
            fade_in_letters: function() {

            	var timings = [0,100,200,300,400,500,700,800,900]
            	var animation_length = [500]
            	animation_length_normal = 50;
            	normal_delay_total = 0;
            	var animate_to_color = '#fff';

            	$(".logo-wrapper svg path").each(function(){
            		var $this_letter = $(this);
            		var rand_delay = timings[Math.floor(Math.random() * timings.length)];
            		var rand_anim = animation_length[Math.floor(Math.random() * animation_length.length)];

            		normal_delay_total = normal_delay_total + animation_length_normal;

            		$this_letter.delay(normal_delay_total).animate({
            			display: 'block'
            		}, {
            			duration: rand_anim,
            			queue: true,
            			complete: function() { /* Animation complete */
                            //$this_letter.css({'fill':animate_to_color});
                            $this_letter.addClass('complete');
                            if ($('.complete').length > 7 && !$('body').hasClass('splash-after-letter-fadein')){
                            	$('body').addClass('splash-after-letter-fadein');
                            	window.site.splash_screen.after_letter_fadein();
                            }
                        }
                    });
            	});

            },
            
            after_letter_fadein: function() {

            	$('.location-wrapper').delay(1000).queue(function() {
            		$('.location-item').each(function(i){
            			var $item = $(this);
            			setTimeout(function() {
            				$item.addClass('visible')
            			}, 200*i);
            			$(this).dequeue();
            		});
            	});

            	setTimeout(function() {
            		if ($('#main_slideshow').length) {
            			var first_slide = $('#main_slideshow .slide').not('.cycle-sentinel').filter(':eq(0)');
            			window.site.layout.hero.change_slide_effects(null, null, null, first_slide, null);
            			$('#main_slideshow').cycle('resume');
            		}
            	}, 3000);

            	setTimeout(function() {
            		window.site.splash_screen.hide_splash();
            	}, 3500);

                //HILMAN
                $('.container_outer').css('visibility', 'visible')
            },

            randomise_letters: function() {


            },
            hide_splash: function() {

            	$('.splash-bg').removeClass('open start');
            	$('body').removeClass('splash-open');

            	$('body').delay(800).queue(function() {
            		$('body').removeClass('active-init');
            		$('body').removeClass('splash-init');
            		$(this).dequeue();
            	});



            }
        },
      
    };

    $(document).ready(function() {

    	window.site.init();

    });


})(jQuery);







