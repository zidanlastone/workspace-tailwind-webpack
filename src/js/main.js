const mixitup = require('mixitup')
const AOS = require('aos');
const getUrl = window.location;
const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
const API = getUrl.protocol + '//' + getUrl.host + '/' +  'API_AZRA/index.php/Azraapps'
window.jQuery = $;
window.$ = $;
window.AOS = AOS

window.onscroll = function() {
let currentScrollPos = window.pageYOffset;
let w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  if(w > 400){
    if (220 > currentScrollPos) {
      document.getElementById("nav-wrapper").style.top = "0";
      document.getElementById("navbar-primary").classList.remove('shadow-sm')
    } else {
      document.getElementById("nav-wrapper").style.top = "-3rem";
      document.getElementById("navbar-primary").classList.add('shadow-sm')
    }
    prevScrollpos = currentScrollPos;
  }
}


window.onresize = function () {
  let pageWidth = window.innerWidth
  if(pageWidth < 425){
    document.getElementById("download-footer").classList.add('order-12')
    document.getElementById("menu-footer").classList.add('order-1')
    document.getElementById("download-container").classList.add('text-center')
    document.getElementById("download-image").classList.add('mx-auto')
  }else{
    document.getElementById("download-footer").classList.add('order-1')
    document.getElementById("menu-footer").classList.add('order-12')
    document.getElementById("download-container").classList.remove('text-center')
    document.getElementById("download-image").classList.remove('mx-auto')
  }
}

$(document).ready(function(){
  // console.log(API, baseUrl)

  if(document.querySelector('#slider-dokter') != null){
    $('#slider-dokter').owlCarousel({
        // nav: true,
        loop: true,
        autoplay: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        navText : ['<i class="fa fa-angle-left mx-2" aria-hidden="true"></i>','<i class="fa fa-angle-right mx-2" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav:true
            }
        }
    })
  }

  if(document.querySelector('#slider-dokter-igd') != null ){
    $('#slider-dokter-igd').owlCarousel({
        // nav: true,
        loop: true,
        autoplay: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        navText : ['<i class="fa fa-angle-left mx-2" aria-hidden="true"></i>','<i class="fa fa-angle-right mx-2" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav:true
            }
        }
    })

  }

  if(document.querySelector('#carousel-penghargaan') != null){
    $('#carousel-penghargaan').owlCarousel({
        responsive: {
            0: {
                items: 1
            }
        }
    })
  }

  AOS.init()

  $(".nav-item a[href='"+location+"']").addClass('active');
  
  $('#select-dokter').select2({ 
      theme: 'bootstrap4'
  })

  $('#CariNamaDokter').select2({ 
      theme: 'bootstrap4'
  })
  
  if(document.querySelector('#bodyPromosi') != null){
    load_paket_promosi(1)
  } 

  if(document.querySelector('#bodyBerita') != null) {
    load_berita(1)
  }

  if(document.querySelector('#bodyArtikel') != null) {
    load_article(1)
  }

  if(document.querySelector('#bodyPKRS') != null) {
    load_pkrs(1)
  }

  if(document.querySelector('#body-karir') != null) {
    load_karir(1);
  }

  
  $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      let page = $(this).data("ci-pagination-page");
      
      if(document.querySelector('#bodyPromosi') != null) {
        load_paket_promosi(page);
      }

      if(document.querySelector('#bodyBerita') != null) {
        load_berita(page);
      }

      if(document.querySelector('#bodyArtikel') != null) {
        load_article(page);
      }

      if(document.querySelector('#bodyPKRS') != null) {
        load_pkrs(page);
      }

      if(document.querySelector('#body-karir') != null) {
        load_karir(page);
      }

      $('html,body').scrollTop(0);
  });

  const containerDokter = document.querySelector('#mixitup-container')
  if(containerDokter != null){
    let mixer = mixitup(containerDokter)
    
    $('#select-dokter').on('change', function(){
      mixer.filter(this.value)
    })
  
    $('#CariNamaDokter').on('change', function(){
      mixer.filter(this.value)
    })
  }

  if(document.querySelector('#kddokter-list') != null){
    $('#kddokter-list').on('change', function(){ 
      console.log(this.value)
      if(this.value != '--Pilih Dokter Spesialis--'){
        document.querySelector('#today-doctor').classList.add('hidden')
      }else{
        console.log('remove hidden', this.value)
        document.querySelector('#today-doctor').classList.remove('hidden')
      }

      let datax = { field1: $("#kdlyn-list").val(), field2: $("#kddokter-list").val() }
      let url =  baseUrl + '/PublicSite/JadwalDokter_AmbilDataDokter'
      $.ajax({ 
        type: "POST",
        url: url,
        data: datax,
        cache: false,
        success: function(html){
          $("#dokter-list").html(html);
        }  
      });
    })
  }
  
  const containerPenunjangMedis = document.querySelector('#mixitup-penunjang-medis')
  if(containerPenunjangMedis != null){
    let mixerpm = mixitup(containerPenunjangMedis)
    $('.pm-filter').on('click', function(e){
      e.preventDefault()
      console.log(this.dataset.filter)
      mixerpm.filter(this.dataset.filter)
    })
  }
  
  if(document.querySelector('#kdlyn-list') != null) {
    $('#kdlyn-list').select2({ 
        theme: 'bootstrap4'
    })
    $('#kdlyn-list').on('change', function(){
      $.ajax({ 
          type: "POST",
					url: baseUrl + "/PublicSite/JadwalDokter_AmbilKodeLayanan",
					data: "kdlyn="+ this.value,
					cache: false,
					success: function(result){
            $("#kddokter-list").html(result);
          }
				});
    })  
  }


  if(document.querySelector('#divpostingmount') != null ){
    let ArrayPostingMount = []
    $.ajax({
      url: API+'/postingmount/',
      type: 'GET',
      // contentType: 'application/json',
      crossDomain: true,
      success: function(data){
        // console.log(data)
        if(data.data != 'False'){
          $.each(data.data, function(index, result){ 
            let url;

            if(result.url == 'agenda'){
              url = `${baseUrl}/agenda-kegiatan/${result.slug_url}`
            }
            else if(result.url == 'berita'){
              url = `${baseUrl}/berita-kegiatan/${result.slug_url}`
            }
            else if(result.url == 'informasi_kesehatan'){
              url = `${baseUrl}/artikel/${result.slug_url}`
            }
            else if(result.url == 'promosi'){
              url = `${baseUrl}/promosi/${result.slug_url}`
            }

            ArrayPostingMount += `
              <a class="flex m-2 hover:no-underline" href="${url}">
                  <img src="${result.gambar}" class="w-1/5 h-100 my-auto m-2" alt="" onerror="imageError(this)">
                  <div class="p-2">
                  <p class="text-sm text-bold hover:text-green-500">${result.judul}</p> <br>
                  <p class="text-xs">${format_datestring(result.tgl)}</p>
                  </div>
              </a>`

          })
          $('#divpostingmount').append(ArrayPostingMount);
        }
      }
    })
  }

  if(document.querySelector('#newpostingmonth') != null){
    let ArrayCountMonthPosting = []
    $.ajax({
      url: API+'/countposting/',
      type: 'GET',
      crossDomain: true,
      success: function(data){
        $.each(data.count, function(index, result){
          ArrayCountMonthPosting += `<li class="my-2">
          <a href="${baseUrl}/${result.url}" class="my-2 hover:no-underline hover:ml-3">${result.kategori}<span class="float-right">(${result.jumlah_post})</span></a>
          </li>`
        })
        $('#newpostingmonth').append(ArrayCountMonthPosting);
  
      }
    })
  }


  function format_datestring(tgl){
    let date = new Date(tgl)
    const monthNames = ["Jan", "Feb", "Maret", "April", "Mei", "Juni", "Juli", "Agust", "Sept", "Okto", "Nov", "Des"];
    return date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear()
  }



  function load_paket_promosi(page){
      let ArrayPromosi = []
      $.ajax({
          url: baseUrl +"/PublicSite/pagination_paket_promosi/"+page,
          method:"GET",
          dataType:"json",
          success:function(data){
              $.each(data.data, function(index, result){
                let kategoripost
                  if(result.kategori != null){
                      kategoripost = result.kategori	
                  }
                  else{
                      kategoripost = '-'
                  }

                  let stringIsi = result.mobile_isi_promosi
                  let cutIsi = stringIsi.substring(0, 200);
                  ArrayPromosi += `
                  <div class="col-md-6 my-4">
                    <div class="card rounded-2xl border-0 bg-transparent">
                    <a href="${baseUrl}/promosi/${result.slug_url}"><img src="${result.gambar_promosi}" class="overflow-hidden w-100 rounded-2xl" alt="" onerror="imageError(this)"></a>
                        <div class="card-body">
                            <div class="card-title">
                            <a href="${baseUrl}/promosi/${result.slug_url}" class="hover:no-underline text-green-400 font-bold"> <h4>${result.judul_promosi}</h4></a>
                            </div>
                            <p class="text-sm">${cutIsi}</p>
                            <a href="${baseUrl}/promosi/${result.slug_url}" class="text-lg hover:no-underline text-blue-300">Baca Lebih Lanjut <span class="fa fa-long-arrow-alt-right"></span></a>
                            <div class="flex mt-2">
                                <a href="#" class="p-2 text-sm hover:no-underline"><i class="fa fa-tag mx-1"></i>${kategoripost}</a>
                                <a href="#" class="p-2 text-sm hover:no-underline">${result.CountComment}<i class="fa fa-comments mx-1"></i>komentar</a>
                            </div>
                        </div>
                    </div>
                  </div>
                  `
              })
              $('#bodyPromosi').empty();
              $('#bodyPromosi').append(ArrayPromosi)
              $('#pagination_link').html(data.pagination_link);
          }
      });
  }

  function load_berita(page){
    let ArrayPromosi = []
    $.ajax({
        url: baseUrl +"/PublicSite/pagination_berita/"+page,
        method:"GET",
        dataType:"json",
        success:function(data){
            $.each(data.data, function(index, result){
              let kategoripost
                if(result.kategori != null){
                    kategoripost = result.kategori	
                }
                else{
                    kategoripost = '-'
                }

                let stringIsi = result.mobile_isi_berita_kegiatan
                let cutIsi = stringIsi.substring(0, 200);
                ArrayPromosi += `
                <div class="col-md-6 my-4">
                  <div class="card rounded-2xl border-0 bg-transparent">
                  <a href="${baseUrl}/berita-kegiatan/${result.slug_url}"><img src="${result.gambar_berita_kegiatan}" class="overflow-hidden w-100 rounded-2xl" alt="" onerror="imageError(this)"></a>
                      <div class="card-body">
                          <div class="card-title">
                          <a href="${baseUrl}/berita-kegiatan/${result.slug_url}" class="hover:no-underline text-green-400 font-bold"> <h4>${result.judul_berita_kegiatan}</h4></a>
                          </div>
                          <p class="text-sm">${cutIsi}</p>
                          <a href="${baseUrl}/berita-kegiatan/${result.slug_url}" class="text-lg hover:no-underline text-blue-300">Baca Lebih Lanjut <span class="fa fa-long-arrow-alt-right"></span></a>
                          <div class="flex mt-2">
                              <a href="#" class="p-2 text-sm hover:no-underline"><i class="fa fa-tag mx-1"></i>${kategoripost}</a>
                              <a href="#" class="p-2 text-sm hover:no-underline">${result.CountComment}<i class="fa fa-comments mx-1"></i>komentar</a>
                          </div>
                      </div>
                  </div>
                </div>
                `
            })
            $('#bodyBerita').empty();
            $('#bodyBerita').append(ArrayPromosi)
            $('#pagination_link').html(data.pagination_link);
        }
    });
  }

  function load_article(page) {
    let ArrayPromosi = []
    $.ajax({
        url: baseUrl +"/PublicSite/pagination_informasi_kesehatan/"+page,
        method:"GET",
        dataType:"json",
        success:function(data){
            $.each(data.data, function(index, result){
              let kategoripost
                if(result.kategori != null){
                    kategoripost = result.kategori	
                }
                else{
                    kategoripost = '-'
                }

                let stringIsi = result.mobile_isi_informasi_kesehatan
                let cutIsi = stringIsi.substring(0, 200);
                ArrayPromosi += `
                <div class="col-md-6 my-4">
                  <div class="card rounded-2xl border-0 bg-transparent">
                  <a href="${baseUrl}/artikel/${result.slug_url}"><img src="${result.gambar_informasi_kesehatan}" class="overflow-hidden w-100 rounded-2xl" alt="" onerror="imageError(this)"></a>
                      <div class="card-body">
                          <div class="card-title">
                          <a href="${baseUrl}/artikel/${result.slug_url}" class="hover:no-underline text-green-400 font-bold"> <h4>${result.judul_informasi_kesehatan}</h4></a>
                          </div>
                          <p class="text-sm">${cutIsi}</p>
                          <a href="${baseUrl}/artikel/${result.slug_url}" class="text-lg hover:no-underline text-blue-300">Baca Lebih Lanjut <span class="fa fa-long-arrow-alt-right"></span></a>
                          <div class="flex mt-2">
                              <a href="#" class="p-2 text-sm hover:no-underline"><i class="fa fa-tag mx-1"></i>${kategoripost}</a>
                              <a href="#" class="p-2 text-sm hover:no-underline">${result.CountComment}<i class="fa fa-comments mx-1"></i>komentar</a>
                          </div>
                      </div>
                  </div>
                </div>
                `
            })
            $('#bodyArtikel').empty();
            $('#bodyArtikel').append(ArrayPromosi)
            $('#pagination_link').html(data.pagination_link);
        }
    });
  }

  function load_pkrs(page) {
    let ArrayPromosi = []
    $.ajax({
        url: baseUrl +"/PublicSite/pagination_pkrs/"+page,
        method:"GET",
        dataType:"json",
        success:function(data){
            $.each(data.data, function(index, result){
              let kategoripost
                if(result.kategori != null){
                    kategoripost = result.kategori	
                }
                else{
                    kategoripost = '-'
                }

                let stringIsi = result.mobile_isi_pkrs
                let cutIsi = stringIsi.substring(0, 200);
                ArrayPromosi += `
                <div class="col-md-6 my-4">
                  <div class="card rounded-2xl border-0 bg-transparent">
                  <a href="${baseUrl}/pkrs/${result.slug_url}"><img src="${result.gambar_pkrs}" class="overflow-hidden w-100 rounded-2xl" alt="" onerror="imageError(this)"></a>
                      <div class="card-body">
                          <div class="card-title">
                          <a href="${baseUrl}/pkrs/${result.slug_url}" class="hover:no-underline text-green-400 font-bold"> <h4>${result.judul_pkrs}</h4></a>
                          </div>
                          <p class="text-sm">${cutIsi}</p>
                          <a href="${baseUrl}/pkrs/${result.slug_url}" class="text-lg hover:no-underline text-blue-300">Baca Lebih Lanjut <span class="fa fa-long-arrow-alt-right"></span></a>
                          <div class="flex mt-2">
                              <a href="#" class="p-2 text-sm hover:no-underline"><i class="fa fa-tag mx-1"></i>${kategoripost}</a>
                              <a href="#" class="p-2 text-sm hover:no-underline">${result.CountComment}<i class="fa fa-comments mx-1"></i>komentar</a>
                          </div>
                      </div>
                  </div>
                </div>
                `
            })
            $('#bodyPKRS').empty();
            $('#bodyPKRS').append(ArrayPromosi)
            $('#pagination_link').html(data.pagination_link);
        }
    });
  }

  function load_karir(page) {
    let ArrayPromosi = []
    $.ajax({
        url: baseUrl +"/PublicSite/pagination_karir/"+page,
        method:"GET",
        dataType:"json",
        success:function(data){
            $.each(data.data, function(index, result){
              let kategoripost
                if(result.kategori != null){
                    kategoripost = result.kategori	
                }
                else{
                    kategoripost = '-'
                }

                let stringIsi = result.mobile_isi_karir
                let cutIsi = stringIsi.substring(0, 200);
                ArrayPromosi += `
                <div class="col-md-6 my-4">
                  <div class="card rounded-2xl border-0 bg-transparent">
                  <a href="${baseUrl}/karir/${result.slug_url}"><img src="${result.gambar_karir}" class="overflow-hidden w-100 rounded-2xl" alt="" onerror="imageError(this)"></a>
                      <div class="card-body">
                          <div class="card-title">
                          <a href="${baseUrl}/karir/${result.slug_url}" class="hover:no-underline text-green-400 font-bold"> <h4>${result.judul_karir}</h4></a>
                          </div>
                          <p class="text-sm">${cutIsi}</p>
                          <a href="${baseUrl}/karir/${result.slug_url}" class="text-lg hover:no-underline text-blue-300">Baca Lebih Lanjut <span class="fa fa-long-arrow-alt-right"></span></a>
                          <div class="flex mt-2">
                              <a href="#" class="p-2 text-sm hover:no-underline"><i class="fa fa-tag mx-1"></i>${kategoripost}</a>
                              <a href="#" class="p-2 text-sm hover:no-underline">${result.CountComment}<i class="fa fa-comments mx-1"></i>komentar</a>
                          </div>
                      </div>
                  </div>
                </div>
                `
            })
            $('#body-karir').empty();
            $('#body-karir').append(ArrayPromosi)
            $('#pagination_link').html(data.pagination_link);
        }
    });
  }


// $.ajax({
// 	url: '/'+baseUrl+'/PublicSite/GetDataPhotoStream/',
// 	type: 'POST',
// 	crossDomain: true,
// 	success: function(data){
// 		$('#mediagalleryphotostream').empty();
// 		var value = JSON.parse(data)
// 		var ArrayMedia = []
// 		$.each(value, function(index, result){
// 			ArrayMedia +=  '<li class="col-md-4 col-sm-4 portfolio-item2" data-id="id-1" data-type="cat-item-1" style="padding-right: 7px;padding-left: 7px;"><span class="image-block img-hover"><a target="_blank" class="image-zoom" href="'+result.path+'" data-gal="prettyPhoto[gallery]" style="padding: 0"><img style="height: 60px" src="'+result.path+'" class="img-fluid w3layouts agileits" alt="Communal"></a></span></li>';
// 		})
// 		$('.mediagalleryphotostream').prepend(ArrayMedia)

// 	}
// })

})





