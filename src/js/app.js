import "../css/main.css"
import "bootstrap/dist/js/bootstrap.bundle"
import "owl.carousel"
import "select2"
import "./main.js"

window.jQuery = $;
window.$ = $;
window.AOS = AOS

var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

export function onImageError(image, type){
    if(type == 'dokter'){
        image.onerror = "";
        image.src = baseUrl + "/assets/img/dokter-blank-wide.jpg";
    }else{
        image.onerror = "";
        image.src = baseUrl +'/assets/img/default_azra.png';
    }
}


